//
// Created by ss on 2/25/18.
//

#ifndef PONG_BASE_ENGINE_H
#define PONG_BASE_ENGINE_H

#include "game_object.h"
#include "menu/menu.h"
#include <list>
#include <utility>

class GameEngine;

class GameEngine {
protected:

  int height;
  int width;
  Point mouse_pos;
  std::list<GameObject *> game_objects;
  std::list<GameObject *> moving_game_objects;
  Menu menu;

  void draw();


  static void drawCallback();

  static void resizeCallback(int width, int height);

  static void mouseMovementCallback(int x, int y);

  void resize(int width, int height);

  void initOpenGl();

  void pixelVertex(double x, double y);

  void drawGameObject(GameObject *to_draw);

  void setTitle(std::string);

  virtual void gameLogic(double elapsed_time)= 0;

public:
  GameEngine();

  void run();

  void resolveCollisions();

  void set_mouse_pos(int x, int y);


  static GameEngine *running_game;

  void coordinateToOpenGl(double &x, double &y);
};

#endif //PONG_BASE_ENGINE_H
