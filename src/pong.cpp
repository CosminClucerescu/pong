//
// Created by ss on 2/25/18.
//



#include <sstream>
#include <tgmath.h>
#include "pong.h"

void topWallHitCallback(GameObject *gameObject) {
  reinterpret_cast<PongGame *>(GameEngine::running_game)->scoreFor(1, gameObject);
}

void bottomWallHitCallback(GameObject *gameObject) {
  reinterpret_cast<PongGame *>(GameEngine::running_game)->scoreFor(2, gameObject);
}

void ballCallBack(GameObject *gameObject) {
  gameObject->setSpeed(gameObject->getSpeed() + 50);
}

void PongGame::gameLogic(double elapsed_time) {
  this->movePlayer1();
  this->movePlayer2(elapsed_time);
}

void PongGame::movePlayer2(double elapsed_time) {
  GameObject *closest_ball = nullptr;
  double distance_to_closest = 0;
  double distance;
  for (auto ball:this->moving_game_objects) {
    if (ball == this->player_2_pad || ball->getDirection() > 180) {
      continue;
    }
    distance = fabs(ball->getCenter().getY() - this->player_2_pad->getCenter().getY());
    if (closest_ball == nullptr || distance < distance_to_closest) {
      closest_ball = ball;
      distance_to_closest = distance;
    }
  }
  Point target_point;
  if (closest_ball != nullptr) {
    target_point = closest_ball->getCenter();
  } else {
    target_point = Point(this->width / 2, this->height);
  }
  if (this->player_2_pad->getSpeed()) {
    if (player_2_pad->getCenter().getX() < target_point.getX()) {
      this->player_2_pad->setDirection(0);
    } else {
      this->player_2_pad->setDirection(180);
    }
  } else {
    this->player_2_pad->setCenter(target_point.getX(), this->player_2_pad->getCenter().getY());
  }
}

void PongGame::scoreFor(int player, GameObject *gameObject) {
  if (gameObject->getLastCollided() == this->player_2_pad)
    return;
  this->increaseScore(player);
  this->resetBalls();
}

void PongGame::initWalls() {
  auto iterator = this->game_objects.begin();
  //left wall
  GameObject *wall = new Square;
  int wall_width = 10;
  //left wall
  wall->setCenter(0, this->height / 2);
  wall->setHeight(this->height);
  wall->setWidth(wall_width);
  this->game_objects.insert(iterator, wall);
  // right wall
  wall = new Square(*wall);
  wall->setCenter(this->width, this->height / 2);
  this->game_objects.insert(iterator, wall);
  //bottom wall
  wall = new Square;
  wall->setCenter(this->width / 2, 0);
  wall->setHeight(1);
  wall->setWidth(this->width);
  this->game_objects.insert(iterator, wall);
  wall->setCallback(&bottomWallHitCallback);
  //top wall
  wall = new Square(*wall);
  wall->setCenter(this->width / 2, this->height);
  wall->setCallback(&topWallHitCallback);
  this->game_objects.insert(iterator, wall);
}

void PongGame::initPlayerPads() {
  auto iterator = this->game_objects.begin();
  int pad_width = 10;
  int pad_length = this->width / 12;
  GameObject *pad = new Pad(0 + pad_length / 2, this->width - pad_length / 2, 0);
  pad->setCenter(this->width / 2, 0);
  pad->setHeight(pad_width * 2);
  pad->setWidth(pad_length);

  this->player_1_pad = pad;
  this->game_objects.insert(iterator, pad);

  //top pad
  pad_length = this->width / 12;
  pad = new Pad(0 + pad_length / 2, this->width - pad_length / 2, this->height);
  pad->setCenter(this->width / 2, 0);
  pad->setWidth(pad_length);
  pad->setHeight(pad_width * 2);
  pad->setSpeed(1500);
  this->moving_game_objects.insert(this->moving_game_objects.end(), pad);
  this->player_2_pad = pad;
}

PongGame::PongGame() {
  this->initWalls();
  this->initBalls();
  this->initPlayerPads();
  this->initMenu();
}

void PongGame::increaseScore(int player) {
  if (player == 1) {
    this->player_1_score++;
  } else if (player == 2) {
    this->player_2_score++;
  }
  std::ostringstream stream;
  stream << this->player_1_score << " - " << this->player_2_score;
  this->setTitle(stream.str());
}

void PongGame::resetBalls() {
  int i = 1;
  for (auto ball:this->moving_game_objects) {
    ball->setCenter(this->width / 2 + i * 2 * ball->getHeight(), this->height / 2);
    ball->setSpeed(2000);
    ball->setDirection(45);
    ++i;
  }
}

void PongGame::movePlayer1() {
  this->player_1_pad->setCenter(this->mouse_pos.getX(), this->player_1_pad->getCenter().getY());
}

void PongGame::initBalls() {
  GameObject *ball = new Square;
  int ball_size = 5;
  ball->setHeight(ball_size);
  ball->setWidth(ball_size);
  ball->setCenter(this->width / 4, this->height / 6);
  for (int i = 0; i < 3; i++) {
    ball = new Square(*ball);
    ball->setCallback(ballCallBack);
    this->moving_game_objects.insert(this->moving_game_objects.end(), ball);
  }
  this->resetBalls();
}

void PongGame::initMenu() {
  this->menu.addItem("Play", nullptr);
}
