//
// Created by Cosmin Clucerescu on 2/21/18.
//

#ifndef PONG_UTILS_H
#define PONG_UTILS_H

class Point {
private:
  double x;
  double y;
public:
  explicit Point(double x = 0, double y = 0);

  void setXY(double x = 0, double y = 0);

  double getX();

  double getY();
};

#endif //PONG_UTILS_H
