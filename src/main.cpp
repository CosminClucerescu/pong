#include <GL/glut.h>
#include <stdexcept>
#include "pong.h"


int main(int argc, char **argv) {
  glutInit(&argc, argv);
  PongGame g;
  g.run();
  return 0;
}