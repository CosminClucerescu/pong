//
// Created by ss on 2/24/18.
//

#ifndef PONG_GAME_OBJECT_H
#define PONG_GAME_OBJECT_H

#include "utils.h"
#include <list>

class GameObject;

typedef void (*GameObjectCallback)(GameObject *gameObject);


class GameObject {
protected:
  Point center; //Position for the center of the object.
  Point previous_center;
  int direction{}; // movement direction in degrees
  double speed{}; // movement speed in speed.
  int height{};
  int width{};
  GameObject *last_collided{};
  GameObjectCallback callback;

  void triggerCallback();

  bool withinCollisionDistance(GameObject *to_check);

  void updateDirection();

  bool collides(GameObject *obj2);

  double diagonalSize();

  double getDistanceTo(GameObject *to_object);

public:
  GameObject();

  GameObject(const GameObject &game_object);

  void setCallback(GameObjectCallback callback);

  virtual void setCenter(double x, double y);

  Point getCenter();

  void setSpeed(double speed);

  double getSpeed();

  void setDirection(int direction);

  int getHeight();

  virtual std::list<Point> getObjectPoints() =0; //Points describing the object relative to its center.
  void setHeight(int height);

  void setWidth(int width);

  void updatePosition(double elapsed_time);

  GameObject *getLastCollided();


  bool doCollide(GameObject *to_check);


  int getDirection();
};

class Square : public GameObject {
public:

  Square();

  explicit Square(const GameObject &copy_from);

  std::list<Point> getObjectPoints() override;
};

#endif //PONG_GAME_OBJECT_H