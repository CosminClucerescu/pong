//
// Created by ss on 2/25/18.
//

#ifndef PONG_PONG_H
#define PONG_PONG_H

#include "game_engine/base_engine.h"
#include "game_engine/game_object.h"

class Pad : public Square {
  int minX{};
  int maxX{};
  int yPos{};
public:
  Pad(int minX, int maxX, int yPos) : Square() {
    this->minX = minX;
    this->maxX = maxX;
    this->yPos = yPos;
  }

  void setCenter(double x, double y) override {
    if (x > maxX) {
      x = maxX;
    } else if (x < minX) {
      x = minX;
    }
    this->center.setXY(x, this->yPos);
  }
};

class PongGame : public GameEngine {
private:
  int player_1_score = 0;
  int player_2_score = 0;
  GameObject *player_1_pad;
  GameObject *player_2_pad;

  void increaseScore(int player);

  void initBalls();

  void initMenu();

  void resetBalls();

  void movePlayer1();

  void movePlayer2(double elapsed_time);

  void initPlayerPads();

  void initWalls();

public:
  PongGame();

  void scoreFor(int player, GameObject *gameObject);

  void gameLogic(double elapsed_time) override;


};

#endif //PONG_PONG_H
