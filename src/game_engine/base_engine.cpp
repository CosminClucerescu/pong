//
// Created by ss on 2/25/18.
//
#include <GL/glut.h>
#include <stdexcept>
#include "base_engine.h"

GameEngine *GameEngine::running_game; // ...
float measure_elapsed_time() {
  static float start = 0;
  if (!start) {
    start = clock();
    return 0;
  }
  float old_start = start;
  start = clock();
  return start - old_start;

}

void GameEngine::mouseMovementCallback(int x, int y) {
  GameEngine::running_game->set_mouse_pos(x, y);
}

void GameEngine::draw() {
  if (!this->menu.isEmpty()) {
    double x = this->mouse_pos.getX();
    double y = this->mouse_pos.getY();
    this->coordinateToOpenGl(x, y);
    if (!this->menu.changed(static_cast<float>(x), static_cast<float>(y))) {
      glutPostRedisplay();
      return;
    }
    glClear(GL_COLOR_BUFFER_BIT);
    this->menu.draw(static_cast<float>(x), static_cast<float>(y));
  } else {
    glClear(GL_COLOR_BUFFER_BIT);
    double elapsed_time = measure_elapsed_time();
    if (elapsed_time > 5000) {
      elapsed_time = 5000;
    }
    this->gameLogic(elapsed_time);

    for (auto &it: this->moving_game_objects) {
      it->updatePosition(elapsed_time);
    }
    this->resolveCollisions();
    for (auto &it : this->game_objects) {
      this->drawGameObject(it);
    }
    for (auto &it : this->moving_game_objects) {
      this->drawGameObject(it);
    }
  }
  glFlush();
  glutPostRedisplay();
}

void GameEngine::coordinateToOpenGl(double &x, double &y) {
  double g_x = (x - this->width / 2.0) / (this->width / 2);
  double g_y = (y - this->height / 2.0) / (this->height / 2);

  if (g_x > 1) {
    g_x = 1;
  }
  if (g_y > 1) {
    g_y = 1;
  }
  if (g_y < -1) {
    g_y = -1;
  }
  if (g_x < -1) {
    g_x = -1;
  }
  x = g_x;
  y = g_y;

}

void GameEngine::pixelVertex(double x, double y) {
  // Set a vertex for the given pixel.
  this->coordinateToOpenGl(x, y);

  glVertex2d(x, y);
}

void GameEngine::drawGameObject(GameObject *to_draw) {
  glBegin(GL_POLYGON);
  std::list<Point> point_list = to_draw->getObjectPoints();
  for (auto &it : point_list) {
    this->pixelVertex(it.getX(), it.getY());
  }
  glEnd();
}

void GameEngine::drawCallback() {
  GameEngine::running_game->draw();
}

void GameEngine::resizeCallback(int width, int height) {
  GameEngine::running_game->resize(width, height);
}

void GameEngine::resize(int width, int height) {
  glutReshapeWindow(this->width, this->height);
}

void GameEngine::initOpenGl() {
  glutInitDisplayMode(GLUT_SINGLE);
  glutInitWindowSize(this->width, this->height);
  glutInitWindowPosition(100, 100);
  glutCreateWindow("Pong.");
  glutReshapeFunc(GameEngine::resizeCallback);
  glutDisplayFunc(GameEngine::drawCallback);
  glutPassiveMotionFunc(GameEngine::mouseMovementCallback);
  glutMainLoop();
}


GameEngine::GameEngine() {
  this->height = 800;
  this->width = 1200;
  if (GameEngine::running_game != nullptr) {
    throw std::runtime_error("Only one game engine can be instantiated at a time.");
  }
  GameEngine::running_game = this;
}

void GameEngine::run() {
  this->initOpenGl();

}

void GameEngine::resolveCollisions() {
  for (auto to_check:this->moving_game_objects) {
    for (auto game_object:this->game_objects) {
      if (game_object->doCollide(to_check)) {
        break;
      }
    }
  }
  for (auto to_check = this->moving_game_objects.begin(); to_check != this->moving_game_objects.end(); ++to_check) {
    for (auto game_object = to_check; game_object != this->moving_game_objects.end(); ++game_object) {
      if ((*game_object)->doCollide(*to_check)) {
        break;
      }
    }
  }
}

void GameEngine::setTitle(std::string new_title) {
  glutSetWindowTitle(new_title.c_str());
}

void GameEngine::set_mouse_pos(int x, int y) {
  this->mouse_pos.setXY(x, y);
}

