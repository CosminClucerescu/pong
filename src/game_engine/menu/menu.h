//
// Created by ss on 3/18/18.
//

#include <list>
#include <string>
#include <utility>
#include <GL/glut.h>

#ifndef PONG_MENU_H
#define PONG_MENU_H

void drawBitmapText(std::string text, float x, float y);

typedef void (*MenuItemCallback)();


class MenuItem {
  MenuItemCallback callback;
  std::string text;
public:
  MenuItem();

  MenuItem(std::string text, MenuItemCallback callback);

  std::string getString() {
    return this->text;
  }

  void setCallback();
};

class Menu {
  std::list<MenuItem> menu_items;
  int highlighted_pos = -1;
public:
  void addItem(std::string text, MenuItemCallback callback) {
    this->menu_items.insert(this->menu_items.end(), MenuItem(std::move(text), callback));
  }

  bool isEmpty() {
    return menu_items.empty();
  }

  bool changed(float mouse_x, float mouse_y) {
    int new_highlit_pos = 0;
    int curr_pos = 1;
    float y = 0.1;
    for (const auto &menu_item:this->menu_items) {
      if (((y - 0.27) < mouse_y) && (mouse_y < (y - 0.18))) {
        new_highlit_pos = curr_pos;
        break;
      }
      curr_pos += 1;
    }
    bool r_val = this->highlighted_pos != new_highlit_pos;
    this->highlighted_pos = new_highlit_pos;
    return r_val;
  }

  void draw(float mouse_x, float mouse_y) {
    float y = 0.1;
    int curr_pos = 1;
    for (auto menu_item:this->menu_items) {
      if (curr_pos == this->highlighted_pos) {
        glColor3f(1, 0, 0);
        drawBitmapText(menu_item.getString(), 0, y);
        glColor3f(1, 1, 1);
      } else {
        drawBitmapText(menu_item.getString(), 0, y);
      }

      y += 0.1;
    }
  }
};

#endif //PONG_MENU_H
