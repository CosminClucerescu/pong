//
// Created by Cosmin Clucerescu on 2/24/18.
//

#include <stdexcept>
#include "utils.h"
#include "game_object.h"
#include <cmath>

#define PI 3.14159265


void GameObject::setCenter(double x, double y) {
  this->previous_center.setXY(this->center.getX(), this->center.getY()); // TODO: init
  this->center.setXY(x, y);
}

Point GameObject::getCenter() {
  return this->center;
}

void GameObject::setDirection(int direction) {
  if (direction > 360 || direction < 0) {
    throw std::runtime_error("Direction should be between 0 and 360");
    //Could also set it to modulo 360 but It'll probably just hide my errors.
  }
  this->direction = direction;
}

void GameObject::setSpeed(double speed) {
  this->speed = speed;
}

double GameObject::getSpeed() {
  return this->speed;
}

void GameObject::setHeight(int height) {
  this->height = height;
}

void GameObject::setWidth(int width) {
  this->width = width;
}

bool GameObject::collides(GameObject *obj2) {
  double d = this->center.getX();
  double d_y = this->center.getY();
  double d_half_width = this->width / 2;
  double d_half_height = this->height / 2;
  double whatever = obj2->center.getX();
  double whatever_y = obj2->center.getY();
  double whatevere_half_height = obj2->height / 2;
  double whatevere_half_width = obj2->width / 2;
  return d - d_half_width < whatever + whatevere_half_width &&
         d + d_half_width > whatever - whatevere_half_width &&
         d_y + d_half_height > whatever_y - whatevere_half_height &&
         d_y - d_half_height < whatever_y + whatevere_half_height;
}

void GameObject::updateDirection() {
  int hit_angle = 180 - 90 - (direction % 90);
  int rotation_helper = direction % 180;
  Point aux = this->center;
  this->center = this->previous_center;
  bool lateral_hit = true;
  if (this->center.getX() > this->last_collided->getCenter().getX()) {
    for (auto last_collided_point:this->last_collided->getObjectPoints()) {
      for (auto previous_point:this->getObjectPoints()) {
        if (previous_point.getX() < last_collided_point.getX()) {
          lateral_hit = false;
        }
      }
      if (!lateral_hit)
        break;
    }
  } else {
    for (auto last_collided_point:this->last_collided->getObjectPoints()) {
      for (auto previous_point:this->getObjectPoints()) {
        if (previous_point.getX() > last_collided_point.getX()) {
          lateral_hit = false;
        }
      }
      if (!lateral_hit)
        break;
    }
  }
  this->center = aux;
  if (lateral_hit) {
    //Lateral hit
    rotation_helper += 90;
    rotation_helper = rotation_helper % 180;
  }
  int rotation = 0;
  if (rotation_helper > 90) {
    rotation = 1;
  }
  if (hit_angle > 0 && hit_angle < 90) {
    if (rotation) {
      direction = hit_angle + (direction - (direction % 90)) + 90;
    } else {
      direction = (direction - (direction % 90)) - 90 + hit_angle;
    }
  } else {
    direction += 180;
  }
  direction = direction % 360;
  if (direction < 0) {
    direction = 360 + direction;
  }
  this->setDirection(direction);
}

void GameObject::updatePosition(double elapsed_time) {
  if (this->speed == 0)
    return;
  Point position = this->getCenter();
  double new_x = position.getX();
  double new_y = position.getY();
  double sin_x = cos((double) direction * PI / 180.0);
  double cos_y = sin((double) direction * PI / 180.0);
  new_x = new_x + ((speed / 500000.0 * (long int) elapsed_time / 10.0) * sin_x);
  new_y = new_y + ((speed / 500000.0 * (long int) elapsed_time / 10.0) * cos_y);
  this->setCenter(new_x, new_y);
}

GameObject *GameObject::getLastCollided() {
  return this->last_collided;
}


GameObject::GameObject(const GameObject &game_object) : center(game_object.center) {
  this->height = game_object.height;
  this->width = game_object.width;
  this->speed = game_object.speed;
  this->direction = game_object.direction;
  this->callback = nullptr;
}

void GameObject::setCallback(GameObjectCallback callback) {
  this->callback = callback;
}

GameObject::GameObject() {
  this->callback = nullptr;
}

int GameObject::getHeight() {
  return this->height;
}

double GameObject::diagonalSize() {
  //Get the diagonal size of the rectagle in which this object is inscribed.
  return sqrt((this->width * this->width) + (this->height * this->height));
}

double GameObject::getDistanceTo(GameObject *to_object) {
  double x = pow((this->center.getX() - to_object->center.getX()), 2);
  double y = pow((this->center.getY() - to_object->center.getY()), 2);
  return sqrt(x + y);
}

bool GameObject::withinCollisionDistance(GameObject *to_check) {

  return (this->getDistanceTo(to_check) < this->diagonalSize() + to_check->diagonalSize());
};

std::list<Point> Square::getObjectPoints() {
  Point point;
  std::list<Point> point_list;
  double center_x = this->center.getX();
  double center_y = this->center.getY();
  point.setXY(center_x - this->width / 2, center_y - this->height / 2); //bottom left
  point_list.insert(point_list.end(), point);

  point.setXY(center_x - this->width / 2, center_y + this->height / 2); //top left
  point_list.insert(point_list.end(), point);

  point.setXY(center_x + this->width / 2, center_y + this->height / 2); //top right
  point_list.insert(point_list.end(), point);

  point.setXY(center_x + this->width / 2, center_y - this->height / 2); //bottom right
  point_list.insert(point_list.end(), point);
  return point_list;
}

void GameObject::triggerCallback() {
  if (this->callback != nullptr) {
    this->callback(this);
  }
}

bool GameObject::doCollide(GameObject *to_check) {
  if ((this == to_check || to_check->getLastCollided() == this) ||
      (this->speed && this->last_collided == to_check)) {
    return false;
  }
  if (!to_check->withinCollisionDistance(this)) {
    //No point in checking if the objects collide if they're not close enough for a collision.
    return false;
  }
  if (to_check->collides(this)) {
    to_check->last_collided = this;
    this->last_collided = to_check;
    to_check->updateDirection();

    if (this->getSpeed()) {
      this->updateDirection();
    }
    this->triggerCallback();
    to_check->triggerCallback();
    return true;
  }
  return false;
}

int GameObject::getDirection() {
  return this->direction;
}

Square::Square(const GameObject &copy_from) : GameObject(copy_from) {

}

Square::Square() : GameObject() {

}