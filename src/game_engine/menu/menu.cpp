//
// Created by ss on 3/18/18.
//
#include "menu.h"

#include <utility>

void MenuItem::setCallback() {

}

MenuItem::MenuItem() {
  this->callback = nullptr;
}

MenuItem::MenuItem(std::string text, MenuItemCallback callback) {
  this->callback = callback;
  this->text = std::move(text);
}

void drawBitmapText(std::string text, float x, float y) {
  glRasterPos3f(x, y, 0);
  for (char &c : text) {
    glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, c);
  }
}
