//
// Created by Clucerescu Cosmin on 2/21/18.
//
#include "utils.h"

Point::Point(double x, double y) {
  this->x = x;
  this->y = y;
}

void Point::setXY(double x, double y) {
  this->x = x;
  this->y = y;
}

double Point::getX() {
  return this->x;
}

double Point::getY() {
  return this->y;
}